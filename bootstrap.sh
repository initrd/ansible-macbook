#!/bin/bash

# exit on command fail and error on unset variable
set -eu

echo "Installing homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "Installing ansible"
brew install ansible

echo "Create ~/Documents/git directory"
mkdir ~/Documents/git

echo "Cloning ansible playbook into ~/Documents/git directory"
git clone https://gitlab.com/initrd/ansible-macbook ~/Documents/git
